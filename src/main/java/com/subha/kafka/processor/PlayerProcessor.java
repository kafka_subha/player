package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by subha on 03/07/2018.
 */

@Component("player")
public class PlayerProcessor {
    private static final Logger LOG = LoggerFactory.getLogger(PlayerProcessor.class);

    @Value("${player.id}")
    String playerId;

    Random random = new Random();

    public void process(Exchange exchange) {
        int seed = Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]);
        LOG.info("Got seed " + seed);
        int guess = random.nextInt(1000)+1;
        LOG.info(playerId + " generated the number=" + guess + " for player=" + playerId + "for game=" + exchange.getIn().getBody().toString().split("-")[1]);
        exchange.getIn().setBody(guess + "-" + playerId + "-" + exchange.getIn().getBody().toString().split("-")[1]);
    }

}
