package com.subha.kafka.route;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */
@Component
public class PlayerRoute extends RouteBuilder {

    @Value("${kafka.channel}")
    private String channel;

    @Value("${kafka.server}")
    private String server;

    @Value("${kafka.port}")
    private String port;

    @Value("${kafka.from-topic}")
    private String topic;

    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:player?level=ERROR&showHeaders=true&showBody=true")
                .useOriginalMessage());

        from("kafka:" + topic + "?brokers=" + server + ":" + port + "&groupId=" + channel + "&autoOffsetReset=earliest&consumersCount=1")
                .routeId("player")
                .to("bean:player?method=process")
                .to("kafka:{{kafka.to-topic}}?brokers={{kafka.server}}:{{kafka.port}}");
    }
}
